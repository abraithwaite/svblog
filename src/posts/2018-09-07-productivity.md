---
date: 2018-09-06T23:59:59Z
title: 'Productivity Mystery'
url: /2018/09/06/productivity-mystery/
---

I listen to Planet Money and The Indicator pretty regularly.  One theme that's
come up a few times is the issue of the productivity mystery.  This was the
topic for The Indicator yesterday.  The mystery is: As technology advances, we
should get more productive.  However in the last few decades we have not
observed that periodic advancement.

The formula is roughly:

Stuff produced / Time spent working

Like mentioned in this episode, I believe there is no mystery.  Below I address
each of the theories in the episode as an armchair economist with absolutely no
background in the subject aside from being an avid listener.

Mismeasurement
--------------

I agree with the theory here.  I'd be surprised if we are measuring production
effectively.  It would be interesting to know how models account for new forms
of products in information and digital assets.

Labor is cheap
--------------

This is probably a cause as well, but for a different reason than mentioned in
the episode.  In the episode, it's proposed that when labor is cheaper than
automation then we favor labor.  However, I believe the reason is more nuanced.

I belive we as humans would rather employ somebody at a low wage than replace
them with a machine.  This is compounded by the fact that organizational
structures are complex.  For example: the CEO of a midmarket or enterprise
company is unlikely to know that a manager hired their friend as a favor over
automating a task.

There is no mystery
-------------------

True, but not for the reasons stated.  Technology _is_ accelerating
productivity.  Unfortunately, cultural and societal expectations are holding us
back from realizing those gains.

We _are_ getting better at producing things, but people aren't cutting back
hours worked.  Think about it, if we get more efficient at producing goods, it
takes fewer people to produce those goods.  Yes, that translates into fewer
hours worked, but it also leads to jobs lost.  But people aren't just going to
stop working.  There are cultural and social forces at work which require that
we keep working.

Welfare in the majority of countries (by population) typically sucks.  You
can't stop working even if you want to because you have to feed yourself.

Lag time
--------

True.  When our culture shifts to accept the fact that not everyone has to work
and support those individuals in that decision, then we'll start to see
productivity gains again.

In conclusion
-------------

Productivity is still advancing, but it's unnoticeable as long as everyone is
still expected to work. I would be interested in hearing about how economists
think about unemployment as it relates to productivity, as it seems logical to
assume that the larger the population of non-employed [1] members of society
there are, the higher I would expect productivity to be.  If we could automate
everything that society needs to operate (food, shelter, etc), what would that
look like in terms of productivity measurement?

[1] Avoiding the term unemployment because of it's implications: must be
looking for work, etc.
