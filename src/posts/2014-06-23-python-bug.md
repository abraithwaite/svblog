---
date: 2014-06-23T23:48:59Z
title: __init__.py can be a directory
url: /2014/06/23/python-bug/
---

I found an interesting [python quirk](http://bugs.python.org/issue21784) the
other day.  Not too serious, but could have some interesting consequences
