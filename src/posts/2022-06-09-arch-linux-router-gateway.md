---
title: Arch Linux Home Router Gateway using systemd-networkd
date: 2022-06-09T16:41:39.625Z
url: arch-linux-home-router-on-systemd-networkd
---

This is part of a series of posts about building your own Linux home router using systemd-networkd. The posts are organized as follows:

1. [Connecting to CenturyLink using PPPoE and systemd-networkd](/centurylink-fiber-pppoe/)
2. [Network Architecture and VLAN configuration](/arch-linux-home-router-on-systemd-networkd/)
3. [Firewalld policy-based access control between zones](/firewalld-home-router-setup-and-configuration/)

This guide assumes you have a working knowledge of Linux, networking and routing concepts. This guide is built on Arch, but should be roughly translatable to other Linux distributions which have Systemd Networkd, Firewalld and pppd packaged at a relatively recent version.

In this post, we'll cover the new home architecture as well as device and VLAN configuration using systemd-networkd.

## Architecture

I love making architecture diagrams as they drastically aid in developing the mental model needed to build the system.

My requirements for this network are fairly simple:

1. I want policy-based access control between subnets/VLANs.
2. I want to centralize DHCP and DNS management.
3. There is no 3.

![](https://slabstatic.com/prod/uploads/g0u3gkxa/posts/images/reyr41BIKc9F3rQn3xmmbZLe.png)

One major drawback of this particular architecture is that we've got a major
bottleneck at the router for inter-vlan traffic.  If we're ever trying to
stream 4k in 60fps on multiple devices, that may become an issue.

## Configuring the Router

As with any linux router, you must enable IP forwarding first. This enables it at boot:

```
cat >/etc/sysctl.d/99-ip_forward.conf<<EOF
net.ipv4.ip_forward = 1
EOF
```

To apply this now, you can run: `sysctl -w net.ipv4.ip_forward = 1`.

Next comes systemd-networkd setup which is beautifully simple.  Systemd-networkd uses the same familiar unit-file syntax that all other systemd services and triggers use and is very readable.

First we should configure the physical network device facing our internal network (`enp3s0`).  In the previous post we configured the internet-facing network adapter (`enp2s0`).  We configure the device as a VLAN trunk using the following config:

```
cat >/etc/systemd/network/enp3s0.network<<EOF
[Match]
Name=enp3s0

[Network]
VLAN=enp3s0.10
VLAN=enp3s0.20
VLAN=enp3s0.30
VLAN=enp3s0.1001
EOF
```

That indicates to systemd-networkd to look for those children devices and configure this device as the parent (VLAN trunk).

Next, we need to configure those virtual network adapters using  `*.netdev` files.  I'm only going to show one example here, since they're all pretty much identical in my setup.  For VLAN 10, that looks like:

```
cat >enp3s0.10.netdev<<EOF
[NetDev]
Name=enp3s0.10
Kind=vlan

[VLAN]
Id=10
EOF
```

After running `networkctl reload`, this file will create the device without any further network configuration.  Of course, we do want network configuration and more.  For my purposes, I'll need a DHCP server and DNS configured on each subnet.  Remember what I mentioned earlier about centralized DHCP? This is what I meant.

```
cat >enp3s0.10.network<<EOF
[Match]
Name=enp3s0.10

[Network]
Address=172.16.10.1/24
DHCPServer=true
IPMasquerade=true

[DHCPServer]
PoolOffset=100
PoolSize=100
EmitDNS=yes
DNS=1.1.1.1
DNS=1.0.0.1
EOF
```

This file defines a number of things:

1. The device's statically allocated address
2. The subnet (derived from the address)
3. The DHCP server settings
4. The DNS settings passed to the clients
5. And IPMasquerade which enables NAT through the router.

This configuration is identical to the other VLANs but with the changed VLAN Tag on the match block to indicate the correct device this network file is configuring.

I like to start DHCP addresses at x.100 because it makes them easy to identify visually.  I'll never have 100 static IPs in use and I also doubt I'll have 154ish dhcp devices connected either so I set the pool size and offset to 100 for convenience.

The other devices and subnets are left as an exercise for the reader!  Hint: you can pretty much just `sed` the vlan tag with the new one for each device.   In the next post we'll cover the most important component of the home network: the firewall.

Coming from `pf` I have pretty high standards, but I'm happy to report that configuring firewalld wasn't as troublesome as I initially imagined.  Stay tuned!  It's gonna be 🔥🔥🔥🧱.
