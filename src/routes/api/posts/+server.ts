import { json } from '@sveltejs/kit'

const fetchMarkdownPosts = async () => {
    const allPostFiles = import.meta.glob('/src/posts/*.md')
    const iterablePostFiles = Object.entries(allPostFiles)
    console.log(allPostFiles)

    const allPosts = await Promise.all(
        iterablePostFiles.map(async ([path, resolver]) => {
            // TODO: figure out why this type isn't resolving
            const { metadata } = await resolver() as {
                metadata: {
                    title: string,
                    date: Date,
                    path: string,
                }
            }
            // hack. '/src/posts/'.length == 11 and '.md'.length == 3
            const postPath = path.slice(11, -3)

            return {
                meta: metadata,
                path: postPath,
            }
        })
    )

    return allPosts
}

export const GET = async () => {
    const allPosts = await fetchMarkdownPosts()

    const sortedPosts = allPosts.sort((a, b) => {
        return b.meta.date > a.meta.date ? -1 : 1
    })

    return json(sortedPosts)
}